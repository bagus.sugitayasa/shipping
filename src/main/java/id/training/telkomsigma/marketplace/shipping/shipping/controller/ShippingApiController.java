/**
 * 
 */
package id.training.telkomsigma.marketplace.shipping.shipping.controller;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version Id: 
 */
@RestController
public class ShippingApiController {
	
	@GetMapping("/api/cost/{from}/{to}/{weight}")
	public Map<String, String> calculateCost(@PathVariable String from, @PathVariable String to, @PathVariable BigDecimal weight){
		
		Map<String, String> count = new HashMap<>();
		count.put("from", from);
		count.put("to", to);
		count.put("weight", String.valueOf(weight));
		count.put("count", String.valueOf(new BigDecimal(20000)));
		return count;
	}
}
